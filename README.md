# Contribution to Ar-PHP Library, namespace-psr4 added

## Install
composer require ibrain/ar-php 

## Usage

    <?php
	use Ibrain\Arabic\I18N_Arabic;
	
		$Arabic = new I18N_Arabic('Numbers'); 
		$integer = 141592653589;
        $text = $Arabic->int2str($integer);
		
Output Will be 'مئة و واحد و أربعون مليار و خمسمئة و إثنان و تسعون مليون و ستمئة و ثلاث و خمسون ألف و خمسمئة و تسع و ثمانون
'		
		
## Support Arabic 
Documentation [PHP Arabic -- Ar-PHP](https://www.ar-php.org/I18N/Arabic/Docs/I18N_Arabic/_Arabic.php.html) 
For more support in using arabic language in php, I think this site deserve a visit:  [PHP Arabic -- Ar-PHP](http://www.ar-php.org/)
